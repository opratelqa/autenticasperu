package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseAutenticasPeru;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class AutenticasPeru extends TestBaseAutenticasPeru {
	
	final WebDriver driver;
	public AutenticasPeru(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInAutenticasPeru(String apuntaA,String ambiente) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Autenticas Peru - landing");
		
	//Seccion Home Publica
	
	WebElement inicio = driver.findElement(By.xpath("//a[contains(text(), 'INICIO')]"));		
	inicio.click();
		String elemento1 = driver.findElement(By.xpath("//*[@id=\"miradas\"]/div/div/div[1]/h1")).getText();
			Assert.assertEquals(elemento1,"Miradas Peruanas");
		System.out.println("Seccion MIRADAS PERUANAS "+"Visualizacion Exitosa");
	espera(500);
	
	//Seccion Miradas Peruanas
	
	WebElement miradasPeruanas = driver.findElement(By.xpath("//a[contains(text(), 'Miradas Peruanas')]"));		
	miradasPeruanas.click();
		String elemento2 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div[1]/h2")).getText();
			Assert.assertEquals(elemento2,"Miradas Peruanas");
		System.out.println("Seccion Miradas Peruanas "+"Visualizacion Exitosa");
	espera(500);
	
	//Seccion Cuidate
	
	WebElement cuidate = driver.findElement(By.xpath("//a[contains(text(), 'Cuídate')]"));		
	cuidate.click();
		String elemento3 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div[1]/h2")).getText();
			Assert.assertEquals(elemento3,"Cuídate");
		System.out.println("Seccion Cuídate "+"Visualizacion Exitosa");
	espera(500);
	
	//Seccion Date un gusto
	
	WebElement dateUnGusto = driver.findElement(By.xpath("//a[contains(text(), 'Date un Gusto')]"));		
	dateUnGusto.click();
		String elemento4 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div[1]/h2")).getText();
			Assert.assertEquals(elemento4,"Date un Gusto");
		System.out.println("Seccion Date un Gusto "+"Visualizacion Exitosa");
	espera(500);
	
	//Seccion inspirate
	
	WebElement inspirate = driver.findElement(By.xpath("//a[contains(text(), 'Inspírate')]"));		
	inspirate.click();
		String elemento5 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div[1]/h2")).getText();
			Assert.assertEquals(elemento5,"Inspírate");
		System.out.println("Seccion Inspírate "+"Visualizacion Exitosa");
	espera(500);
	
	//Seccion inspirate
	
	WebElement paraMamas = driver.findElement(By.xpath("//a[contains(text(), 'Para Mamás')]"));		
	paraMamas.click();
		String elemento6 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div[1]/h2")).getText();
			Assert.assertEquals(elemento6,"Para Mamás");
		System.out.println("Seccion Para Mamás "+"Visualizacion Exitosa");
	espera(500);
	
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Autenticas Peru - landing");
			
	}			
		
}